package hu.sprint.ts.greeting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TsGreetingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsGreetingApplication.class, args);
	}

}

package hu.sprint.ts.currency.conversion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@ToString
public class CurrencyConversion {

    private Long id;

    private String fromCurrency;

    private String toCurrency;

    private BigDecimal exchangeRate;

    private String exchangeEnvironmentInfo;

    private String conversionEnvironmentInfo;

    private BigDecimal amount;

    private BigDecimal calculatedValue;


    public CurrencyConversion(String fromCurrency, String toCurrency, BigDecimal exchangeRate) {
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.exchangeRate = exchangeRate;
    }

    public CurrencyConversion(Long id, String fromCurrency, String toCurrency, BigDecimal exchangeRate,
                              BigDecimal amount, BigDecimal calculatedValue, String exchangeEnvironmentInfo,
                              String conversionEnvironmentInfo) {
        this.id = id;
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.exchangeRate = exchangeRate;
        this.exchangeEnvironmentInfo = exchangeEnvironmentInfo;
        this.conversionEnvironmentInfo = conversionEnvironmentInfo;
        this.amount = amount;
        this.calculatedValue = calculatedValue;
    }
}

package hu.sprint.ts.currency.conversion;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@Slf4j
public class CurrencyConversionController {

    @Autowired
    private InstanceInformationService instanceInformationService;


    @Autowired
    private CurrencyExchangeServiceProxy proxy;

    @GetMapping("/")
    public String imHealthy() {
        return "{healthy:true}";
    }


    @GetMapping("/ts-currency-conversion/from/{from}/to/{to}/amount/{amount}")
    @ResponseStatus(HttpStatus.OK)
    public CurrencyConversion convertCurrency(@PathVariable String from, @PathVariable String to,
                                              @PathVariable BigDecimal amount) {

        log.info("Received Request to convert from {} {} to {}. ", amount, from, to);

        CurrencyExchange response = proxy.retrieveExchangeValue(from, to);

        BigDecimal calculatedValue = amount.multiply(response.getExchangeRate());

        String conversionEnvironmentInfo = instanceInformationService.retrieveInstanceInfo();

        return new CurrencyConversion(response.getId(), from, to, response.getExchangeRate(), amount,
                calculatedValue, response.getEnvironmentInfo(),  conversionEnvironmentInfo);
    }
}

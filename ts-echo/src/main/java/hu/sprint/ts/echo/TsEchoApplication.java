package hu.sprint.ts.echo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TsEchoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsEchoApplication.class, args);
	}

}

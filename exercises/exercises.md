# Exercises

## Exercise 1

1. Run a nginx, a mysql, and a httpd (apache) server
2. Run all of them –detach (or -d), name them with –name
3. nginx should listen on 9080:80, httpd on 8080:80, mysql on 3306:3306
4. When running mysql, use the –env option (or -e) to pass in MYSQL_RANDOM_ROOT_PASSWORD=yes
5. Use docker container logs on mysql to find the random password it created on startup
6. Clean it all up with docker container stop and docker container rm (both can accept multiple names or ID’s)
7. Use docker container ls to ensure everything is correct before and after cleanup

[Solution 1](exercise1.md)

## Exercise 2

1. Use curl in containers
2. Use centos: 7 and ubuntu:22.04
3. Use the docker container -rm option
4. Ensure curl is installed ubuntu: apt-get update && apt-get install curl and centos: yum update curl
5. Check curl -version

[Solution 2](exercise2.md)

## Exercise 3

1. Use curl in containers
2. Use centos: 7 and ubuntu:14.04
3. Use the docker container -rm option
4. Ensure curl is installed ubuntu: apt-get update && apt-get install curl and centos: yum update curl
5. Check curl -version

[Solution 3](exercise3.md)


## Exercise 5

1. Create a postgres container with named volume psql-data using version 9.6.1
2. Use Docker Hub to learn VOLUME path and versions needed to run it
3. Check logs, stop container
4. Create a new postgres container with same named volume using 9.6.2
5. Check logs to validate

[Solution 5](exercise5.md)


## Exercise 6

1. Build a basic compose file for a Drupal content management system website.
2. Use the drupal image along with the postgres image
3. Use ports to expose Drupal on 8080 so you can localhost:8080
4. Be sure to set POSTGRES_PASSWORD for postgres
5. Drupal setup via browser
6. Use volumes to store Drupal unique data

[Solution 6](exercise6.md)

## Exercise 7
- Run a keycloak server in docker container
- Run a MSSQL server 2022 in docker container
- Run a Wordpress website in docker container
- Run a MariaDB server in docker container
- Run Payara server full profile in docker container






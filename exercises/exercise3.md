## Exercise 2 solutions

```bash
docker network create sprint

docker container run -d --net sprint --net-alias search elasticsearch:2

docker container run -d --net sprint --net-alias search elasticsearch:2

docker container ls

docker container run --rm --net sprint alpine nslookup search

docker container run --rm --net sprint centos curl -s search:9200

docker container ls

docker container rm -f 

```

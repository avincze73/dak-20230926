# Setup Ubuntu 22.04
- Ubuntu 22.04 server can installed as virtual machine using virtual box, vmware or other virtualization solution.
- Download ubuntu server image from here: \
https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-live-server-amd64.iso?_ga=2.22971134.438869203.1693916096-453674183.1693916096
- Install the server with bridge network. The vm should access the internet and the host should connect to the vm.


## Post installation tasks
```bash
#execute commands as root user
adduser sprintadm
usermod -aG sudo sprintadm

#execute commands as sprintadm user
sudo apt update && sudo apt upgrade -y &&
    sudo timedatectl set-timezone Europe/Budapest &&
    sudo apt install -y default-jdk maven zsh &&
    sudo usermod -s $(which zsh) sprintadm
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

mkdir git && cd git && git config --global credential.helper store
```

## Installing docker
```bash
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common &&
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg &&
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null &&
    sudo apt update &&
    sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose &&
    sudo usermod -aG docker sprintadm &&
    sudo systemctl restart docker
```

## Installing k3s
```bash
sudo curl -sfL https://get.k3s.io | sh -s -  --write-kubeconfig-mode 644 server --cluster-init
sudo cat /var/lib/rancher/k3s/server/node-token

sudo curl -sfL https://get.k3s.io | K3S_TOKEN=K1014028616d057c3d74ad0c5d818cb9ff02e3e5e7011d90d7f7b0f0ac11fbfda6e::server:304693a25a36613fa6811011506c9e7d \
  sh -s - --write-kubeconfig-mode 644 server --server https://appserver1-sprint:6443
```

## Installing rancher with helm
```bash
sudo curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="v1.26.8+k3s1" sh -s -  --write-kubeconfig-mode 644 server --cluster-init

sudo cp /etc/rancher/k3s/k3s.yaml .kube/config
sudo chmod 744 .kube/config
sudo vim .kube/config

sudo snap install helm --classic
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
kubectl create namespace cattle-system
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.11.1/cert-manager.crds.yaml
helm repo add jetstack https://charts.jetstack.io
helm repo update
#sudo helm fetch jetstack/cert-manager --version v1.7.1


helm install cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.11.1


helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=dbserver-sprint \
  --set replicas=1 \
  --set bootstrapPassword=Sprint12345!

echo https://dbserver-sprint/dashboard/?setup=$(kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}')

kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{ "\n" }}'

sudo kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole cluster-admin \
  --user admin

echo https://dbserver-sprint/dashboard/?setup=Sprint12345!



  sudo kubectl apply -f https://manager-dev/v3/import/xnhqnff6b5wjj2d4tw6knc544ql82s7c52kz6qdfppc5wb4d27gndn_c-m-m8r9v6bz.yaml
  or
  sudo curl --insecure -sfL https://manager-dev/v3/import/xnhqnff6b5wjj2d4tw6knc544ql82s7c52kz6qdfppc5wb4d27gndn_c-m-m8r9v6bz.yaml | kubectl apply -f -

```



[Főoldal](../README.md)


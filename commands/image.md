# Container Images, Where To Find Them and How To Build Them

- App binaries and dependencies
- Not a complete OS. No kernel, no drivers. 
- THe host provides the kernel.


## The Mighty Hub: Using Docker Hub Registry Images

```bash
http://hub.docker.com

docker image ls

docker pull nginx

docker pull nginx:1.11.9

docker pull nginx:1.11

docker pull nginx:1.11.9-alpine

docker image ls
```

## Images and Their Layers: Discover the Image Cache

```bash
docker image ls

# Image layers
# Check the file sizes
# One layer represents one command
# An image is a collection of image chunks
docker history nginx:latest

docker history mysql

docker image inspect nginx

```

## Image Tagging and Pushing to Docker Hub
```bash
docker image tag -- help

docker image ls

docker pull mysql/mysql-server

docker image ls

docker pull nginx:mainline

docker image ls

# You can download one image with the tags
# An image does not have a name
docker image tag nginx avincze73/nginx

docker image tag --help

docker image ls

docker image push avincze73/nginx

docker --help

docker login

cat .docker/config.json

docker image push avincze73/nginx

docker image tag avincze73/nginx avincze73/nginx:testing

docker image push avincze73/nginx:testing

docker image ls

docker image push avincze73/nginx:testing

docker image ls
```


## Building Images: The Dockerfile Basics

```bash
cd dockerfile-sample-1

vim Dockerfile

```
<!-- ## Building Images: Running Docker Builds

```bash
docker image build -t customnginx .

docker image ls

docker image build -t customnginx .
``` -->

## Building Images: Extending Official Images

```bash
cd dockerfile-sample-2

vim Dockerfile

docker container run -p 9080:80 --rm nginx

docker image build -t nginx-with-html .

docker container run -p 9081:80 --rm nginx-with-html

docker image ls

docker image tag --help

docker image tag nginx-with-html:latest aincze73/nginx-with-html:latest

docker image ls

docker push

docker system df
docker kill $(docker ps -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q)
docker volume rm $(docker volume ls -q)

docker container prune
docker image prune
docker system prune

```
# Introduction 

## Docker Topics

- Docker basics
- Image basics
- Docker networking
- Docker volumes
- Docker compose


> A project contains the training material of the course.

## Table of contents

- [Post installation tasks of Ubuntu 20.04](ubuntu/README.md)
- [Azure Cloud Services](cloud/README.md)
- [Setup Azure CLI](azure/README.md)
- [Deploying the echo pod the AKS cluster](echo/README.md)

- [Microservice architecture](diagram/README.md)

- [Kubernetes in depth](ts-greeting/README.md)
- [Web Application](ts-ems/README.md)

- [Ingress](ingress/README.md)
- [Istio](istio/README.md)

- [Further samples](k8s/README.md)



Dive in to [TS-Greeting](ts-greeting/README.md).

package hu.sprint.ts.currency.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"hu.sprint.ts.currency.exchange"})
@EntityScan(basePackages = {"hu.sprint.ts.currency.exchange"})
@EnableJpaRepositories(basePackages = {"hu.sprint.ts.currency.exchange"})
public class TsCurrencyExchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsCurrencyExchangeApplication.class, args);
	}

}

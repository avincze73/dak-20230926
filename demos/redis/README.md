
## Using dockerfile
```bash
#Building the redis image
docker build --progress=plain  .
docker run IMAGEID
#Explain the image creation mechanism through redis and gcc
#Explain rebuild with cache
#Replace the order of redis and gcc installation
#Explain how to tag an image and use custom image names
docker build -t avincze73/redis-server:latest . 
docker build -t avincze73/myredis:v1 .
```


## How to create docker image without dockerfile
```bash
docker run -it alpine sh
apk add --update redis
#From another shell
docker ps
docker commit -c 'CMD ["redis-server"]' CONTAINERID
docker run c25a2226fa0fd7
```


## Installing portainer on docker
```bash
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```


## Useful docker commands
```bash

#docker commands

#clean up any resources — images, containers, volumes, and networks — that are dangling
docker system prune

#To additionally remove any stopped containers and all unused images (not just dangling images), 
#add the -a flag to the command
docker system prune -a

#Remove one or more specific images
docker images -a
docker rmi Image1 Image2


#Remove dangling images
#Dangling images are layers that have no relationship to any tagged images. 
#They no longer serve a purpose and consume disk space.
docker images -f dangling=true
docker images purge

#Removing images according to a pattern
docker images -a |  grep "pattern"
docker images -a | grep "pattern" | awk '{print $3}' | xargs docker rmi


#Remove all images
docker images -a
docker rmi $(docker images -a -q)

#Remove one or more specific containers
docker ps -a
docker container ls -a
docker rm ID_or_Name ID_or_Name

#Run and Remove
docker run --rm image_name


#Remove all exited containers
docker ps -a -f status=exited
docker rm $(docker ps -a -f status=exited -q)


#Stopping all containers
docker stop $(docker ps -a -q)

#Removing all containers
docker rm $(docker ps -a -q)

#Removing all images
docker rmi $(docker images -q) --force


## List Docker containers (running, all, all in quiet mode)
docker container ls
docker container ls --all
docker container ls -aq


#Running docker image
docker run -d -p 4000:80 -e NAME=Attila friendlyhello

#Executing docker image in interactive mode
docker run -it myimage
```

const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send('Hi from Sprint Academy');
});

app.listen(9001, () => {
    console.log('Listening on port 8080');
});
